import psycopg2
from shapely.wkt import dumps
from shapely.geometry import Polygon
import time


class GisQuery:
    def __init__(self):
        self.connection = psycopg2.connect(database="gis", user="docker", password="rTjoP3ukbQ6NqJiHq9", port="25432", host="localhost")
        self.cur = self.connection.cursor()

    def getIntersects(self, poly):
        """
        Returns Data intersecting poly
        """
        self.cur.execute("select gid, id, imk_areal, journalnr, cvr, marknr, afgroede, afgkode, gbanmeldt, gb from agr where ST_Intersects(geom, ST_GeomFromText('{}'));".format(dumps(poly)))
        return self.cur.fetchall()

    def getClassOccurrence(self, poly):
        """
        Returns a list of tuples (afgkode, afgroede, #occurrence) intersecting poly
        """
        self.cur.execute("select  afgkode, afgroede, count(*) from agr where ST_Intersects(geom, ST_GeomFromText('{}')) group by afgroede, afgkode;".format(dumps(poly)))
        rows=[]
        for row in self.cur.fetchall():
            rows.append((int(row[0]), row[1], row[2]))
        return rows

    def getArealSum(self, poly):
        """
        Returns sum of imk_areal intersecting poly
        """
        self.cur.execute("select sum(imk_areal) from agr where ST_Intersects(geom, ST_GeomFromText('{}'));".format(dumps(poly)))
        return self.cur.fetchone()[0]


    def __del__(self): 
        self.cur.close()
        self.connection.close()



q = GisQuery()
poly= Polygon([(9.72020, 55.90300), (9.72020, 55.99300), (9.80020, 55.90300)]) #EPSG:4326

"""
start = time.time()
r = q.getIntersects(poly) #ohne index 0.269s, mit 0.036s
ende = time.time()
print('{:5.3f}s'.format(ende-start))
"""

print(q.getClassOccurrence(poly))

print(q.getArealSum(poly))










